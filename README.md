## Installation
### Database
1. Using the 'Database.sql' script, create the database in SQL Server Management Studio, by opening and executing it.
2. Create a user on the server called 'shopper' with password 'xxxx'.
3. Give 'shopper' the 'db_datareader' & 'db_datawriter' roles on the 'Shop' database in shopper>properties>user mapping.
4. Make sure that 'SQL and Windows Authentication' is enabled.

### Website
1. Open a cmd window and cd to the root directory of the site (the folder with this README).
2. Run `npm install` (If it fails, try it again).
3. Then once the modules are installed, run `npm start`.
4. Browse to `localhost:8080` in your browser (developed using chrome).
5. Logging will be done into the cmd window.
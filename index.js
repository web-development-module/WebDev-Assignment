// https://scotch.io/courses/build-an-online-shop-with-vue

var express = require('express')
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')
var session = require('express-session')
var path = require('path')

// invoke an instance of express application.
var app = express()

// Set server port
var serverPort = 8080

// Set the view engine to ejs
app.set('view engine', 'ejs')

// Static folder for uploads
app.use('/uploads', express.static(path.join(__dirname + '/uploads')))

// Initialize cookie-parser so we can see cookies in the browser
app.use(cookieParser())

// Initialize body-parser to parse incoming form data into req.body
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

// Initialize express session
app.use(session({
    key: 'user_sid',
    secret: 'asdfopsdfkopfdfkglp',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}))

// Check for cookies and logout if no username
app.use(function (req, res, next) {
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie('user_sid')
    }
    next()
})

// Middleware function to check for logged in users
var sessionChecker = function (req, res, next) {
    if (req.session.user && req.cookies.user_sid) {
        res.redirect('/items')
    } else {
        next()
    }
}

// Route for Home-Page
app.get('/', sessionChecker, function (req, res) {
    res.redirect('/items')
})

// Route for user signup
app.route('/signup')
    .get(sessionChecker, function (req, res) {
        res.render('pages/signup')
    })
    .post(require('./app/auth/create_user_validator') ,require('./app/auth/create_user_controller'))

// Route for user Login
app.route('/login')
    .get(sessionChecker, function (req, res) {
        res.render('pages/login', {
            error: ''
        })
    })
    .post(require('./app/auth/login_controller'))
    
// Route for user logout
app.get('/logout', function (req, res) {
    if (req.session.user && req.cookies.user_sid) {
        res.clearCookie('user_sid')
        res.redirect('/')
    } else {
        res.redirect('/login')
    }
})

// For the user's profile use this router
app.use('/profile', require('./app/profile/profile_router'))

// For items routes use the this router
app.use('/items', require('./app/items/items_router'))


// Route for 404s
app.use(function (req, res) {
    res.status(404).render('pages/404')
})

// start the express server
app.listen(serverPort, function () {
    console.log('Server listening on port: ' + serverPort)
})
module.exports = (function () {

    // Module imports
    const sql = require('mssql')

    // Private members - DB LOGIN DETAILS
    var _config = {
        user: 'shopper',
        password: 'xxxx',
        server: 'localhost',
        database: 'Shop',
    }
    var _pool

    // Trigger connect at module load
    sql.connect(_config).then(function (pool) {
        console.log('Database connection pool created')
        _pool = pool
    }).catch(function (err) {
        console.log('Database error creating connection pool')
        console.log(err)
    })

    // Global error handler
    sql.on('error', function (err) {
        console.log('General error in database driver')
        console.log(err)
    })

    // Module exports
    return _pool

})()
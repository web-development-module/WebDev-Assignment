module.exports = (function () {

    // Module imports
    var datalayer = require('../datalayer')
    var moment = require('moment')

    function controller(req, res) {
        if (req.session.user && req.cookies.user_sid) {

            // Get a single item using its id
            datalayer.get_item(req.params.id).then(function (result) {
                var item = result.recordset[0]
                item.Posted = moment.unix(item.Posted).format('MM/DD/YYYY')

                // Check if logged in user is item owner
                if (req.session.user.id === item.OwnerID) {

                    // Get categories to fill dropdown
                    datalayer.get_categories().then(function (result) {
                        var categories = result.recordset

                        res.render('pages/edit_item', {
                            item: item,
                            categories: categories
                        })
                    }).catch(function (err) {
                        console.log('get categories err: ' + err)
                    })
                } else {
                    return res.status(401).render('pages/error', {
                        message: 'Edit disallowed. You are not the owner of this item.'
                    })
                }
            }).catch(function (err) {
                console.log('get single item err: ' + err)
            })
        } else {
            res.redirect('/login')
        }
    }

    // Module exports
    return controller

})()

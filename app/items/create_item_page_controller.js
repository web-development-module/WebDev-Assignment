module.exports = (function () {

    // Module imports
    var datalayer = require('../datalayer')

    function controller(req, res) {
        if (req.session.user && req.cookies.user_sid) {

            // Get all categories
            datalayer.get_categories().then(function (result) {
                var categories = result.recordset

                res.render('pages/create_item', {
                    categories: categories
                })
            }).catch(function (err) {
                console.log('get categories err: ' + err)
            })
        } else {
            res.redirect('/login')
        }
    }

    // Module exports
    return controller

})()
module.exports = (function () {
    
        // Module imports
        var datalayer = require('../datalayer')
    
        function controller(req, res) {
            if (req.session.user && req.cookies.user_sid) {
    
                // Get the item to delete using the ID
                datalayer.get_item(req.params.id).then(function (result) {
                    var item = result.recordset[0]
                    if (req.session.user.id == item.OwnerID) {
                        
                        // Delete an item (Change Deleted=1 in db)
                        datalayer.delete_item(item.ID).then(function (result) {
                            if (result.rowsAffected == 1) {
                                console.log('item deleted successfully')
                                res.redirect('/profile')
                            } else {
                                console.log('item delete failed')
                            }
                        }).catch(function (err) {
                            console.log('item delete err: ' + err)
                        })
                    } else {
                        return res.status(401).render('pages/error', {
                            message: 'Delete disallowed. You are not the owner of this item.'
                        })
                    }
                }).catch(function (err) {
                    console.log('get item err: ' + err)
                })
            } else {
                res.redirect('/login')
            }
        }
    
        // Module exports
        return controller
    
    })()
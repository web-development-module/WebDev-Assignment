module.exports = (function () {

    // Module imports
    const {
        check
    } = require('express-validator/check')
    const {
        sanitize
    } = require('express-validator/filter')

    var _validationchain = [
        check('title').isLength({
            min: 4,
            max: 50
        }),
        check('description').isLength({
            min: 4
        }),
        check('condition').isLength({
            min: 3,
            max: 4
        }),
        check('category_id').isInt({
            min: 1,
            max: 9
        }),
        check('price').isFloat({
            min: 0.01
        }),
        sanitize('title').trim(),
        sanitize('description').trim(),
        sanitize('condition').trim(),
        sanitize('category_id').toInt()
    ];

    // Module exports
    return _validationchain

})()
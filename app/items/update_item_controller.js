module.exports = (function () {

    // Module imports
    var datalayer = require('../datalayer')
    const {
        validationResult
    } = require('express-validator/check')

    function controller(req, res) {
        if (req.session.user && req.cookies.user_sid) {

            // Validate request
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                console.log(errors.mapped())
                return res.status(422).render('pages/error', {
                    message: 'Invalid input. Please check all fields before trying to submit again.'
                })
            }

            // Get a single item to check if the user attemping to update the item is its owner
            datalayer.get_item(req.params.id).then(function (result) {
                var item = result.recordset[0]
                if (req.session.user.id == item.OwnerID) {
                    var updatedItem = {
                        id: req.params.id,
                        title: req.body.title,
                        description: req.body.description,
                        price: req.body.price,
                        condition: req.body.condition,
                        category_id: req.body.category_id,
                    }

                    // Update an item using the detials provided
                    datalayer.update_item(updatedItem).then(function (result) {
                        if (result.rowsAffected == 1) {
                            console.log('item updated successfully')
                            res.redirect('/items/' + updatedItem.id)
                        } else {
                            console.log('update item failed')
                        }
                    }).catch(function (err) {
                        console.log('update item err: ' + err)
                    })
                } else {
                    return res.status(401).render('pages/error', {
                        message: 'Edit disallowed. You are not the owner of this item.'
                    })
                }
            }).catch(function (err) {
                console.log('get-item err: ' + err)
            })
        } else {
            res.redirect('/login')
        }
    }

    // Module exports
    return controller

})()
module.exports = (function (){
    
        // Module imports
        var datalayer = require('../datalayer')
    
        function controller(req, res) {
            if (req.session.user && req.cookies.user_sid) {

                // Buy an item
                datalayer.buy_item(req.params.id).then(function(result) {
                    if (result.rowsAffected == 1) {
                        console.log('item created succesfully')

                        // Get the details of the item that was just purchased
                        datalayer.get_item(req.params.id).then(function(result) {
                            var item = result.recordset[0]
                            delete item.Password

                            res.render('pages/buy_item', {
                                item: item
                            })
                        }).catch(function(err) {
                            console.log('get item err: ' + err)
                            res.redirect('/items')
                        })
                        
                    } else {
                        console.log('buy item failed')
                        res.redirect('/items')
                    }
                }).catch(function(err) {
                    console.log('buy item err: ' + err)
                })

            } else {
                res.redirect('/login')
            }
        
        }
    
        // Module exports
        return controller
    
    })()
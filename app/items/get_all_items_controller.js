module.exports = (function () {

    // Module imports
    var datalayer = require('../datalayer')
    var moment = require('moment')

    function controller(req, res) {

            // Get all items
            datalayer.get_all_items().then(function (result) {
                var items = result.recordset

                // Format the date and price of items
                for (var n = 0; n < items.length; n++) {
                    items[n].Posted = moment.unix(items[n].Posted).format('DD/MM/YYYY')
                    items[n].Price = items[n].Price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    delete items[n].Password
                }
                res.render('pages/items', {
                    items: items,
                    search: ''
                })
            }).catch(function (err) {
                console.log('get all items err: ' + err)
            })
    }

    // Module exports
    return controller

})()
module.exports = (function () {
    
        // Module imports
        const {
            sanitize
        } = require('express-validator/filter')
        const {
            check
        } = require('express-validator/check')
    
        var _validationchain = [
            check('search').not().isEmpty(),
            sanitize('search').trim()
        ];
    
        // Module exports
        return _validationchain
    
    })()
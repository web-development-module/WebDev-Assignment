module.exports = (function () {

    // Module imports
    var datalayer = require('../../datalayer')
    var moment = require('moment')
    const {
        validationResult
    } = require('express-validator/check')

    function controller(req, res) {
            // Validate request
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).render('pages/error', {
                    message: 'Invalid input.'
                })
            }

            // Search for an item using the search string
            datalayer.search_items(req.body.search).then(function (result) {
                var items = result.recordset
                var itemsForSale = [];

                // Format the date and price of all returned items
                for (var n = 0; n < items.length; n++) {
                    items[n].Posted = moment.unix(items[n].Posted).format('DD/MM/YYYY')
                    items[n].Price = items[n].Price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    delete items[n].Password

                    // Remove sold items
                    if(items[n].Status == 'Sale') {
                        itemsForSale.push(items[n])
                    }
                }
                res.render('pages/items', {
                    items: itemsForSale,
                    search: req.body.search
                })
            }).catch(function (err) {
                console.log('get all items err: ' + err)
            })
    }

    // Module exports
    return controller

})()
module.exports = (function () {

    // Module imports
    var datalayer = require('../datalayer')
    var moment = require('moment')

    function controller(req, res) {

        // Get a single item
        datalayer.get_item(req.params.id).then(function (result) {
            var item = result.recordset[0]
            item.Posted = moment.unix(item.Posted).format('MM/DD/YYYY')
            item.Price = item.Price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            delete item.Password

            // Check if logged in user is item owner to see if the buy button should be enabled
            item.Disabled = ''
            if (req.session.user) {
                if (req.session.user.id === item.OwnerID) {
                    item.Disabled = 'disabled'
                }
            } else {
                item.Disabled = 'disabled'
            }

            res.render('pages/item', {
                item: item
            })
        }).catch(function (err) {
            console.log('get single item err: ' + err)
        })

    }

    // Module exports
    return controller

})()
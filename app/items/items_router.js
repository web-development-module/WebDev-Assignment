module.exports = (function () {

    // Module imports
    var express = require('express')
    var datalayer = require('../datalayer')
    var path = require('path')
    var multer = require('multer')
    
    // Configuring multer for storing the item image
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, path.join(__dirname, '/../../uploads'))
        },
        filename: function (req, file, cb) {
            var filename = Date.now()
            // Append the file type back on the file
            switch (file.mimetype) {
                case 'image/png':
                    filename = filename + ".png";
                    break;
                case 'image/jpeg':
                    filename = filename + ".jpeg";
                    break;
                default:
                    break;
            }
            cb(null, filename)
        }
    })
    var upload = multer({
        storage: storage
    })

    var router = express.Router()

    // Route for list of items
    router.get('/', require('./get_all_items_controller'))

    // Route for listing items based on the search
    router.post('/search',require('./search/search_validator') ,require('./search/search_controller'))

    // Route for getting the item creation page
    router.get('/create', require('./create_item_page_controller'))
    
    // Route for creating an item
    router.post('/create', upload.single('image'), require('./create_item_validator'), require('./create_item_controller'))
    
    // Route for a single item
    router.get('/:id', require('./get_item_controller'))
    
    // Route to get the item data for the update form
    router.get('/:id/edit', require('./update_item_page_cotroller'))

    // Route to update an item's data
    router.post('/:id/edit', require('./update_item_validator'), require('./update_item_controller'))

    // Route to delete an item
    router.get('/:id/delete', require('./delete_item_controller'))

    // Route to buy an item
    router.get('/:id/buy', require('./buy_item_controller'))

    // Module exports
    return router

})()
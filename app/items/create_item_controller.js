module.exports = (function () {

    // Module imports
    datalayer = require('../datalayer')
    const {
        validationResult
    } = require('express-validator/check')

    function controller(req, res) {
        if (req.session.user && req.cookies.user_sid) {

            // Validate request
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).render('pages/error', {
                    message: 'Invalid input. Please check all fields before trying to submit again.'
                })
            }

            var item = {
                title: req.body.title,
                description: req.body.description,
                price: req.body.price,
                condition: req.body.condition,
                category_id: req.body.category_id,
                status: 'Sale',
                posted: (Date.now() / 1000), // Current unix time
                owner_id: req.session.user.id,
                attachment: req.file.filename
            }
            
            // Create the item
            datalayer.create_item(item).then(function (result) {
                if (result.rowsAffected == 1) {
                    console.log('item created succesfully')
                    res.redirect('/items')
                } else {
                    console.log('item creation failed')
                }
            }).catch(function (err) {
                console.log('item creation err: ' + err)
            })
        } else {
            res.redirect('/login')
        }
    }

    // Module exports
    return controller

})()
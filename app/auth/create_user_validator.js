module.exports = (function () {

    // Module imports
    const {
        check
    } = require('express-validator/check')

    var _validationchain = [
        check('username').isLength({
            min: 1,
            max: 20
        }),
        check('email').isEmail(),
        check('mobile').isMobilePhone("en-GB"),
        check('postcode').isPostalCode("GB"),
    ];

    // Module exports
    return _validationchain

})()
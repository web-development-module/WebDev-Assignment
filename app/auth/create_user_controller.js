module.exports = (function () {

    // Module imports
    var auth_datalayer = require('./auth_datalayer')
    const {
        validationResult
    } = require('express-validator/check')

    function controller(req, res) {

        // Validate request
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(422).render('pages/error', {
                message: 'Invalid input. Please check all fields before trying to submit again.'
            })
        }

        var newUser = {
            username: req.body.username,
            email: req.body.email,
            mobile: req.body.mobile,
            postcode: req.body.postcode,
            password: req.body.password
        }

        // Create a new user
        auth_datalayer.create_user(newUser).then(function (result) {
            if (result.rowsAffected == 1) {

                var loginDetails = {
                    username: req.body.username,
                    password: req.body.password
                }

                // After creating the user, retrieve it from the db to get the auto generated ID
                auth_datalayer.find_user(loginDetails, function (user) {
                    if (user) {
                        req.session.user = user
                        res.redirect('/items')
                    } else {
                        res.redirect('/signup')
                    }
                }).catch(function(err) {
                    console.log('find user err: ' + err)
                })
            } else {
                res.redirect('/signup')
            }
        }).catch(function(err){
            // This error code indicates the username already exists in the database
            if(err.number == 2627) {
                res.render('pages/error', {
                    message: 'That username is already taken'
                })
            } else {
                console.log('user creation err: ' + err.message)
            }
        })
    }

    // Module exports
    return controller

})()
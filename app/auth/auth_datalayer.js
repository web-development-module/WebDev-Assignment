module.exports = (function () {

    var bcrypt = require('bcrypt')
    var sql = require('mssql')
    var connection = require('../database_connection')

    // Find a user from the database using their username and compare their passsword with the entered one
    function find_user(loginDetails, callback) {
        console.log('auth: find user ' + loginDetails.username)

        var query = 'SELECT TOP 1 * FROM Users WHERE Username=@username COLLATE SQL_Latin1_General_CP1_CS_AS;'
        var request = new sql.Request(connection)
        request
            .input('username', sql.VarChar, loginDetails.username)
            .query(query).then(function (result) {
                if (result.rowsAffected == 1) {
                    var row = result.recordsets[0][0]

                    // Check if password is correct
                    if (bcrypt.compareSync(loginDetails.password, row.Password)) {
                        var user = {
                            id: row.ID,
                            username: row.Username,
                            email: row.Email,
                            mobile: row.Mobile,
                            postcode: row.Postcode
                        }
                        callback(user)
                    } else {
                        callback(false)
                    }
                } else {
                    callback(false)
                }
            })
    }

    // Create a new user with provided details
    function create_user(user) {
        console.log('auth: create user ' + user.username)

        // Create hash/salted password
        user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync())

        var query = 'INSERT INTO Users (Username, Email, Mobile, Postcode, Password) VALUES (@username, @email, @mobile, @postcode, @password)'
        var request = new sql.Request(connection)
        return request
            .input('username', sql.VarChar, user.username)
            .input('email', sql.VarChar, user.email)
            .input('mobile', sql.VarChar, user.mobile)
            .input('postcode', sql.VarChar, user.postcode)
            .input('password', sql.VarChar, user.password)
            .query(query)
    }

    // Update a user's detials including password
    function update_user(user) {
        console.log('auth: update user ' + user.username)

        // Create hash/salted password to put in the database
        user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync())
     
        var query = 'UPDATE Users SET Email=@email, Mobile=@mobile, Postcode=@postcode, Password=@password WHERE Username=@username COLLATE SQL_Latin1_General_CP1_CS_AS;'
        var request = new sql.Request(connection)
        return request
            .input('username', sql.VarChar, user.username)
            .input('email', sql.VarChar, user.email)
            .input('mobile', sql.VarChar, user.mobile)
            .input('postcode', sql.VarChar, user.postcode)
            .input('password', sql.VarChar, user.password)
            .query(query)
    }

    // Module exports
    return {
        find_user: find_user,
        create_user: create_user,
        update_user: update_user
    }

})()
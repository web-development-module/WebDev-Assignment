module.exports = (function () {

    // Module imports
    var auth_datalayer = require('./auth_datalayer')

    function controller (req, res) {
        var loginDetails = {
            username: req.body.username,
            password: req.body.password
        }

        // Get the user from the datbase
        auth_datalayer.find_user(loginDetails, function (user) {
            if (user) {
                req.session.user = user
                res.redirect('/items')
            } else {
                // If the user objecy isn't resturned the details were wrong
                return res.status(422).render('pages/login', {
                    error: 'Username or Password incorrect'
                })
            }
        })
    }

    // Module exports
    return controller

})()
module.exports = (function () {
    
        // Module imports
        const {
            check
        } = require('express-validator/check')
    
        var _validationchain = [
            check('email').isEmail(),
            check('mobile').isMobilePhone("en-GB"),
            check('postcode').isPostalCode("GB"),
        ];
    
        // Module exports
        return _validationchain
    
    })()
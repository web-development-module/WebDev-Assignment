module.exports = (function () {

    // Module imports
    var datalayer = require('../datalayer')
    var moment = require('moment')

    function controller(req, res) {
        if (req.session.user && req.cookies.user_sid) {

            // Get all items for the specified user
            datalayer.get_users_items(req.session.user.username).then(function (result) {
                items = result.recordset

                // Format thedate and price of the items
                for (var n = 0; n < items.length; n++) {
                    items[n].Posted = moment.unix(items[n].Posted).format('DD/MM/YYYY')
                    items[n].Price = items[n].Price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }

                res.render('pages/profile', {
                    username: req.session.user.username,
                    email: req.session.user.email,
                    mobile: req.session.user.mobile,
                    postcode: req.session.user.postcode,

                    items: items
                })

            }).catch(function (err) {
                console.log('get items for user ' + req.session.user.username, +' err: ' + err)
            })
        } else {
            res.redirect('/login')
        }
    }

    // Module exports
    return controller
    
})()
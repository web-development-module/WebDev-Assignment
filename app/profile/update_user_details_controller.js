module.exports = (function () {

    // Module imports
    var datalayer = require('../datalayer')
    var auth_datalayer = require('../auth/auth_datalayer')
    const {
        validationResult
    } = require('express-validator/check')

    function controller(req, res) {
        if (req.session.user && req.cookies.user_sid) {

            // Validate request
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).render('pages/error', {
                    message: 'Invalid input. Please check all fields before trying to submit again.'
                })
            }
            
            var loginDetails = {
                username: req.session.user.username,
                password: req.body.current_password
            }

            // Check if current password is correct before making changes to account
            auth_datalayer.find_user(loginDetails, function (user) {
                if (user) {
                    var updatedUser = {
                        username: loginDetails.username,
                        email: req.body.email,
                        mobile: req.body.mobile,
                        postcode: req.body.postcode,
                    }

                    // If a new password is provided, update the database with that
                    if (req.body.new_password) {
                        updatedUser.password = req.body.new_password
                    } else {
                        updatedUser.password = req.body.current_password
                    }

                    // Update the user details using the provided object
                    auth_datalayer.update_user(updatedUser).then(function (result) {
                        req.session.user.email = updatedUser.email
                        req.session.user.mobile = updatedUser.mobile
                        req.session.user.postcode = updatedUser.postcode
                        res.redirect('/profile')
                    }).catch(function (err) {
                        console.log('update user ' + loginDetails.username, +' err: ' + err)
                    })
                } else {
                    res.redirect('/profile')
                }
            })
        } else {
            res.redirect('/login')
        }
    }

    // Module exports
    return controller

})()
module.exports = (function () {

    // Module imports
    var express = require('express')
    var datalayer = require('../datalayer')

    var router = express.Router()

    // Route for getting all items created by a user
    router.get('/', require('./get_users_items_controller'))

    // Route for updating a user's details
    router.post('/', require('./update_user_details_validator'), require('./update_user_details_controller'))

    // Module exports
    return router

})()
module.exports = (function () {

    // Module imports
    var sql = require('mssql')
    var connection = require('./database_connection')

    // Get all items for a user
    function get_users_items(username) {
        console.log('datalayer: get items for user ' + username)
        var query = "SELECT * FROM Items " +
            "INNER JOIN ItemImages ON Items.ID = ItemImages.ItemID " +
            "INNER JOIN Users ON Items.OwnerID = Users.ID " +
            "WHERE Username=@username AND Deleted=0;"
            
        var request = new sql.Request(connection)
        return request
            .input('username', sql.VarChar, username)
            .query(query)
    }

    // Get a specific item
    function get_item(item_id) {
        console.log('datalayer: get item ' + item_id)
        var query = 'SELECT * FROM Items ' +
            'INNER JOIN Categories ON Items.CategoryID = Categories.ID ' +
            'INNER JOIN ItemImages ON Items.ID = ItemImages.ItemID ' +
            "INNER JOIN Users ON Items.OwnerID = Users.ID " +
            'WHERE Items.ID=@item_id AND Deleted=0;'

        var request = new sql.Request(connection)
        return request
            .input('item_id', sql.Int, item_id)
            .query(query)
    }

    // Buy an item - Set it to Sold
    function buy_item(item_id) {
        console.log('datalayer: buy item ' + item_id)
        var query = 'UPDATE Items SET ' +
        'Status=\'Sold\' ' +
        'WHERE ID=@item_id;'

        var request = new sql.Request(connection)
        return request
            .input('item_id', sql.Int, item_id)
            .query(query)
    }

    // Get all of the items items for sale
    function get_all_items() {
        console.log('datalayer: get all items')
        var query = "SELECT * FROM Items " +
        "INNER JOIN Categories ON Items.CategoryID = Categories.ID " +
        "INNER JOIN ItemImages ON Items.ID = ItemImages.ItemID " +
        "INNER JOIN Users ON Items.OwnerID = Users.ID " +
        "WHERE Items.Status='Sale' " +
        "AND Items.Deleted=0;"

        var request = new sql.Request(connection)
        return request.query(query)
    }

    // Create an item
    function create_item(item) {
        console.log('datalayer: create an item')
        var query = 'BEGIN TRANSACTION ' +
            'INSERT INTO Items (Title, Description, Price, Condition, Status, Posted, CategoryID, OwnerID, Deleted) ' +
            'OUTPUT inserted.ID, @attachment INTO ItemImages (ItemID, Attachment) ' +
            'VALUES (@title, @description, @price, @condition, @status, @posted, @category_id, @owner_id, 0) ' +
            'COMMIT TRANSACTION;'

        var request = new sql.Request(connection)
        return request
            .input('title', sql.VarChar, item.title)
            .input('description', sql.VarChar, item.description)
            .input('price', sql.VarChar, item.price)
            .input('condition', sql.VarChar, item.condition)
            .input('status', sql.VarChar, item.status)
            .input('posted', sql.BigInt, item.posted)
            .input('category_id', sql.Int, item.category_id)
            .input('owner_id', sql.Int, item.owner_id)
            .input('attachment', sql.VarChar, item.attachment)
            .query(query)
    }

    // Update and item
    function update_item(updatedItem) {
        console.log('datalayer: update item ' + updatedItem.id)
        var query = 'UPDATE Items SET ' +
            'Title=@title, Description=@description, Price=@price, Condition=@condition, CategoryID=@category_id ' +
            'WHERE ID=@item_id;'

        var request = new sql.Request(connection)

        return request
            .input('title', sql.VarChar, updatedItem.title)
            .input('description', sql.VarChar, updatedItem.description)
            .input('price', sql.VarChar, updatedItem.price)
            .input('condition', sql.VarChar, updatedItem.condition)
            .input('category_id', sql.Int, updatedItem.category_id)
            .input('item_id', sql.Int, updatedItem.id)
            .query(query)
    }

    // Delete an item
    function delete_item(item_id) {
        console.log('datalayer: delete item ' + item_id)
        var query = 'UPDATE Items SET ' +
            'Deleted=1 ' +
            'WHERE ID=@item_id;'

        var request = new sql.Request(connection)
        return request
            .input('item_id', sql.Int, item_id)
            .query(query)
    }

    // Search for items using the provided search criteria
    function search_items(search) {
        console.log('datalayer: search items using: ' + search)
        var query = "SELECT * FROM Items " +
            "INNER JOIN Categories ON Items.CategoryID = Categories.ID " +
            "INNER JOIN ItemImages ON Items.ID = ItemImages.ItemID " +
            "INNER JOIN Users ON Items.OwnerID = Users.ID " +
            "WHERE Items.Status='Sale' " +
            "AND Items.Deleted=0" +
            "AND Items.Title LIKE '%' + @search + '%' " +
            "OR Categories.Category LIKE '%' + @search + '%' " +
            "OR Users.Postcode LIKE '%' + @search + '%';"

        var request = new sql.Request(connection)
        return request
            .input('search', sql.VarChar, search)
            .query(query)
    }

    // Get all categories
    function get_categories() {
        console.log('datalayer: get all categories')
        var query = 'SELECT * FROM Categories;'

        var request = new sql.Request(connection)
        return request.query(query)
    }

    //Module exports
    return {
        get_users_items: get_users_items,
        get_all_items: get_all_items,
        get_item: get_item,
        buy_item: buy_item,
        create_item: create_item,
        update_item: update_item,
        delete_item: delete_item,
        get_categories: get_categories,
        search_items: search_items
    }

})()